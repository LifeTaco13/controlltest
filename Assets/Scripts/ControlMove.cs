// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/ControlMove.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @ControlMove : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @ControlMove()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""ControlMove"",
    ""maps"": [
        {
            ""name"": ""ControllGeneric"",
            ""id"": ""67aa0fd7-f331-48ee-9220-9a65830ec5ae"",
            ""actions"": [
                {
                    ""name"": ""walkup"",
                    ""type"": ""Button"",
                    ""id"": ""6cca1550-105c-4108-881a-98f9a380b7df"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""walkdown"",
                    ""type"": ""Button"",
                    ""id"": ""f69203a4-9cf2-45ad-af3a-d404094042b6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""walkleft"",
                    ""type"": ""Button"",
                    ""id"": ""120a6c7a-e57b-4685-b232-7bc35a3702fe"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""walkright"",
                    ""type"": ""Button"",
                    ""id"": ""aee94167-824e-4a98-96c6-970871afe3f5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""jump"",
                    ""type"": ""Button"",
                    ""id"": ""da0501b7-351a-4f24-8ce4-b21c86dda601"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""specialpunch"",
                    ""type"": ""Button"",
                    ""id"": ""bda72d10-8aa8-49d1-bd97-bf5deed686bb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""5ef41f25-4a19-4a90-be1b-46651e789658"",
                    ""path"": ""<XInputController>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkup"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""70b7aa1e-0ee2-44aa-8dbc-eb3010a30708"",
                    ""path"": ""<Joystick>/stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkup"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aef577d3-050e-4888-a0a3-e3ad6b6bb010"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkup"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""03bed6b9-9f6e-4bc2-81d4-5b0a227e0c36"",
                    ""path"": ""<XInputController>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkdown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c5dfbc05-13cb-40f2-bc34-1eb916a79d19"",
                    ""path"": ""<Joystick>/stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkdown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3d5ddb88-8219-4029-9e65-deb6d9444132"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkdown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae0bcd3b-e708-4865-a574-9f7437623925"",
                    ""path"": ""<XInputController>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkleft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10f109f8-1bc1-4e1f-9759-d1095a2108a6"",
                    ""path"": ""<Joystick>/stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkleft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d2bf5807-4eab-47b6-89de-9ba725d5eb20"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkleft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cc227ddf-e93f-411d-accb-2063ee64da6c"",
                    ""path"": ""<XInputController>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkright"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e52da95a-bbb1-4cc0-b62b-0a8d3ce2df72"",
                    ""path"": ""<Joystick>/stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkright"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""db57a8f1-61bb-4488-b326-0945fe424e73"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""walkright"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d59a020d-96dd-417b-ab5f-3f2767b57c27"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9d66f24d-9a83-473d-b3e1-eb32597a26eb"",
                    ""path"": ""<HID::DragonRise Inc.   Generic   USB  Joystick  >/button3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""efe8e9d7-d351-41e7-9a85-918435ae8326"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3e5eaad-8e17-461f-a5df-04c8803d2945"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""specialpunch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6cb64ae2-141b-4391-9643-2ae6a504e0e3"",
                    ""path"": """",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""specialpunch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // ControllGeneric
        m_ControllGeneric = asset.FindActionMap("ControllGeneric", throwIfNotFound: true);
        m_ControllGeneric_walkup = m_ControllGeneric.FindAction("walkup", throwIfNotFound: true);
        m_ControllGeneric_walkdown = m_ControllGeneric.FindAction("walkdown", throwIfNotFound: true);
        m_ControllGeneric_walkleft = m_ControllGeneric.FindAction("walkleft", throwIfNotFound: true);
        m_ControllGeneric_walkright = m_ControllGeneric.FindAction("walkright", throwIfNotFound: true);
        m_ControllGeneric_jump = m_ControllGeneric.FindAction("jump", throwIfNotFound: true);
        m_ControllGeneric_specialpunch = m_ControllGeneric.FindAction("specialpunch", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // ControllGeneric
    private readonly InputActionMap m_ControllGeneric;
    private IControllGenericActions m_ControllGenericActionsCallbackInterface;
    private readonly InputAction m_ControllGeneric_walkup;
    private readonly InputAction m_ControllGeneric_walkdown;
    private readonly InputAction m_ControllGeneric_walkleft;
    private readonly InputAction m_ControllGeneric_walkright;
    private readonly InputAction m_ControllGeneric_jump;
    private readonly InputAction m_ControllGeneric_specialpunch;
    public struct ControllGenericActions
    {
        private @ControlMove m_Wrapper;
        public ControllGenericActions(@ControlMove wrapper) { m_Wrapper = wrapper; }
        public InputAction @walkup => m_Wrapper.m_ControllGeneric_walkup;
        public InputAction @walkdown => m_Wrapper.m_ControllGeneric_walkdown;
        public InputAction @walkleft => m_Wrapper.m_ControllGeneric_walkleft;
        public InputAction @walkright => m_Wrapper.m_ControllGeneric_walkright;
        public InputAction @jump => m_Wrapper.m_ControllGeneric_jump;
        public InputAction @specialpunch => m_Wrapper.m_ControllGeneric_specialpunch;
        public InputActionMap Get() { return m_Wrapper.m_ControllGeneric; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ControllGenericActions set) { return set.Get(); }
        public void SetCallbacks(IControllGenericActions instance)
        {
            if (m_Wrapper.m_ControllGenericActionsCallbackInterface != null)
            {
                @walkup.started -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkup;
                @walkup.performed -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkup;
                @walkup.canceled -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkup;
                @walkdown.started -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkdown;
                @walkdown.performed -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkdown;
                @walkdown.canceled -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkdown;
                @walkleft.started -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkleft;
                @walkleft.performed -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkleft;
                @walkleft.canceled -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkleft;
                @walkright.started -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkright;
                @walkright.performed -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkright;
                @walkright.canceled -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnWalkright;
                @jump.started -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnJump;
                @jump.performed -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnJump;
                @jump.canceled -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnJump;
                @specialpunch.started -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnSpecialpunch;
                @specialpunch.performed -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnSpecialpunch;
                @specialpunch.canceled -= m_Wrapper.m_ControllGenericActionsCallbackInterface.OnSpecialpunch;
            }
            m_Wrapper.m_ControllGenericActionsCallbackInterface = instance;
            if (instance != null)
            {
                @walkup.started += instance.OnWalkup;
                @walkup.performed += instance.OnWalkup;
                @walkup.canceled += instance.OnWalkup;
                @walkdown.started += instance.OnWalkdown;
                @walkdown.performed += instance.OnWalkdown;
                @walkdown.canceled += instance.OnWalkdown;
                @walkleft.started += instance.OnWalkleft;
                @walkleft.performed += instance.OnWalkleft;
                @walkleft.canceled += instance.OnWalkleft;
                @walkright.started += instance.OnWalkright;
                @walkright.performed += instance.OnWalkright;
                @walkright.canceled += instance.OnWalkright;
                @jump.started += instance.OnJump;
                @jump.performed += instance.OnJump;
                @jump.canceled += instance.OnJump;
                @specialpunch.started += instance.OnSpecialpunch;
                @specialpunch.performed += instance.OnSpecialpunch;
                @specialpunch.canceled += instance.OnSpecialpunch;
            }
        }
    }
    public ControllGenericActions @ControllGeneric => new ControllGenericActions(this);
    public interface IControllGenericActions
    {
        void OnWalkup(InputAction.CallbackContext context);
        void OnWalkdown(InputAction.CallbackContext context);
        void OnWalkleft(InputAction.CallbackContext context);
        void OnWalkright(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnSpecialpunch(InputAction.CallbackContext context);
    }
}
