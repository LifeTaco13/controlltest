﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class WalkCharacter : MonoBehaviour
{
    ControlMove control;


    public void Awake()
    {
        control = new ControlMove();

        control.ControllGeneric.walkup.performed += ctx => InUp();
        control.ControllGeneric.walkdown.performed += ctx => InDown();
        control.ControllGeneric.walkleft.performed += ctx => InLeft();
        control.ControllGeneric.walkright.performed += ctx => InRight();
        control.ControllGeneric.jump.performed += ctx => InA();
        control.ControllGeneric.specialpunch.performed += ctx => InY();

    }

    public void InUp()
    {        
         Debug.Log("Up");        
    }

    public void InDown()
    {
        Debug.Log("Down");
    }

    public void InLeft()
    {        
        Debug.Log("Left");
    }

    public void InRight()
    {        
        Debug.Log("Right");
    }

    public void InA()
    {
        Debug.Log("A");
    }

    public void InY()
    {
        Debug.Log("Y");
    }

    private void OnEnable()
    {
        control.ControllGeneric.Enable();
    }

    private void OnDisable()
    {
        control.ControllGeneric.Disable();
    }

}
